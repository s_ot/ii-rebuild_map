"use strict";

let gulp = require('gulp');
let less = require('gulp-less');

gulp.task('default', ()=>{
	console.log('hoge');
});

gulp.task('less', ()=>{
    gulp.src('html/styles/less/*.less')
    .pipe(less())
    .pipe( gulp.dest('html/styles/css/') );
});

gulp.task('watch', ()=>{
    gulp.watch('html/styles/less/*.less', ['less']);
});