(function(){

    var markerList = {
        "type": "FeatureCollection",
        "features":  [
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [139.7025054, 35.655416]
                },
                "properties": {
                    "name":       "シャレー",
                    "postalcode": "150-0031",
                    "address":    "東京都渋谷区桜丘町８−１７",
                    "image":      "https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/10984213_911981675490013_448234651240849310_n.png?oh=a20ecacf5e8011556619786c91e49d91&oe=580158E2"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [139.7125054, 35.655316]
                },
                "properties": {
                    "name":       "ダミー",
                    "postalcode": "000-0000",
                    "address":    "東京都",
                    "image":      ""
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [139.7125004, 35.659310]
                },
                "properties": {
                    "name":       "ダミー2",
                    "postalcode": "000-0001",
                    "address":    "東京都2",
                    "image":      ""
                }
            }
        ]
    };

    mapboxgl.accessToken = 'pk.eyJ1Ijoic2hvaGVpb3QiLCJhIjoiY2lwOWJ6dDZvMDB4a2VhbnJ4ZGplazR5OCJ9.mJuRYrpnnnImoqmeN5o9Qg';
    var map = new mapboxgl.Map({
        container: 'map_box', // container id
        style: 'mapbox://styles/shoheiot/cip9cyhbe003pahnf0jvsqu1j', //stylesheet location
        // center: [-90, 39.745433], // starting position
        center: [139.7025054, 35.655416], // lon, lat
        zoom: 16 // starting zoom
    });
    map.addControl( new mapboxgl.Geocoder({container: 'geocoder_container'}) );


    map.on('load', function(){
        map.addSource('markers-src', {
            type: 'geojson',
            data: markerList
        });
        map.addLayer({
            id: 'markers-layer',
            type: 'symbol',
            // type: 'circle',
            source: 'markers-src',
            layout: {
                'icon-image': 'star-15',
                'icon-allow-overlap': true
            }
        });
    });

    map.on('click', function(e){
        var features = map.queryRenderedFeatures(e.point, {layers: ['markers-layer']});
        if(!features.length){
            console.log('何もない');
            return false;
        }
        var ppup = PopupMan.new(features[0]);
        // console.log( ppup );
    });

    map.on('mousemove', function(e){
        var features = map.queryRenderedFeatures(e.point, {layers: ['markers-layer']});
        map.getCanvas().style.cursor = (features.length) ? 'pointer': 'default';
    });

////////////////////////////////////////

    var vModel = new Vue({
        el: '#vue',
        data: {
            features: markerList.features
        },
        methods: {
            changeLocation: function(feature){
                map.flyTo({
                    center: feature.geometry.coordinates
                });
                PopupMan.new( feature );
            }
        }
    });

    var PopupMan = {
        // _opened: [],
        new: function(feature){
            var popupHtml = '<div class="my-popup">';
            popupHtml     += (''+feature.properties.image).length ? '<div><img src="'+feature.properties.image+'"></div>' : '';
            popupHtml     += '<p>'+feature.properties.name+'</p>';
            popupHtml     += '<p>〒 '+feature.properties.postalcode+'</p>';
            popupHtml     += '<p>住所: '+feature.properties.address+'</p>';
            popupHtml     += '</div>';
            var p = new mapboxgl.Popup()
                .setLngLat( feature.geometry.coordinates )
                .setHTML( popupHtml )
                .addTo(map);
            return p;
        }
    };

})();
